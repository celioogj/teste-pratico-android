# Challenge Android

## Sobre o Projeto

Nesse desafio, você deve construir um aplicativo que exiba uma lista de filmes e ao selecionar um filme seja exibida uma tela de detalhes. Você é livre para escolher o layout das telas. Abaixo iremos descrever o que deve ser implementado em cada uma das telas. Esperamos que você dê o seu melhor. 

### Tela de Principal 

#### O que deve ser feito

* Mostrar em cada célula o filme e a imagem correspondente com mínimo de título, data e imagem e gênero.
* A documentação da api pode ser acessada no [link](https://developers.themoviedb.org/3/getting-started/introduction)
* Criar um botão para mudar a ordenação de data para crescente / decrescente.
    * Ordenação dos filmes apenas da primeira página (Sem paginação).

#### Onde buscar os dados
* Buscar a lista de filmes no endpoint: https://api.themoviedb.org/3/movie/upcoming?api_key=1f54bd990f1cdfb230adb312546d765d&language=pt-BR&page=1
* Buscar a lista de Gêneros no endpoint: https://api.themoviedb.org/3/genre/movie/list?api_key=1f54bd990f1cdfb230adb312546d765d&language=pt-BR
* Url base imagens: https://image.tmdb.org/t/p/w185/

### Tela de Detalhes 

#### O que deve ser feito

* Mostrar o mínimo de título, imagem, data, sinopse.

### Favoritar

#### O que deve ser feito

* Cada filme deve ser possuir um botão para favoritar.
* A tela na qual este requisito deve ser implementado fica a sua escolha.
* Um filme favorito poderá ser visualizado independente de conexão.
* Deve ser criada uma forma para exibir que o filme é favorito ou não.

---

### Requisitos mínimos

* Desenvolver na linguagem Kotlin
* O aplicativo não pode dar crash
* Boas práticas de programação 
    * Exemplos:
        * Modularização
        * Organização
        * Nome de variável        
* Boas práticas de Orientação a objetos

### Bônus

* Utilizar algum tipo de arquitetura MVC, MVP, MVVM, MVI.
* Architecture Components
    * Exemplo:
        * Livedata
        * Databinding
        * ViewModel
        * Room
        * Navigation
* Tratamento de erro de conexão
* Incluir testes unitários
* Boas práticas de layout
    * Exemplos:
        * Utilização do dimens, colors, strings
        * Principios do material design
        * Utilização do Contraint Layout
* Clean code
* Princípios SOLID
* Controle de estados
    * Exemplo:
        * Ao girar a tela o aplicativo salva a posição do scroll.
* Bottom menu para exibir os filmes favoritos


### Bônus Master

* Incluir testes de interface
* Injeção de dependência
* Fazer o tratamento de segurança da api_key
* Animações
* CI/CD






